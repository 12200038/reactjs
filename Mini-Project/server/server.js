const PORT = process.env.PORT ?? 8000;
const cors = require('cors')
const express = require('express')
const app = express()
const pool = require('./db')
const { v4: uuidv4 } = require('uuid')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const multer = require('multer');

// const uploadToken = multer({ dest: 'uploads/' });
// uploading images
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  }
});

const fileFilter = function (req, file, cb) {
  const allowedMimes = ['image/jpeg', 'image/png', 'image/gif'];
  if (allowedMimes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new Error('Invalid file type. Only JPEG, PNG, and GIF files are allowed.'), false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 20 // 20 MB
  },
  fileFilter: fileFilter,
}).fields([{ name: 'images', maxCount: 20 }]);



app.use(cors())
app.use(express.json())
app.use('/uploads', express.static('uploads'));

// signup
app.post('/signup',async(req,res)=>{

  const {user_email,password} = req.body

  // to hash the password
  const salt = bcrypt.genSaltSync(10)
  console.log(salt,password)
  
  const hashedPassword = bcrypt.hashSync(password,salt)

  try {
      const signUp = await pool.query(`INSERT INTO users (email,hashed_password) VALUES($1,$2);`,
      [user_email,hashedPassword])

      //password token
      const token = jwt.sign({user_email},'secret',{expiresIn:'1hr'})

      res.json({user_email,token})

  } catch (error) {
      console.error(error)
  }
})
//reset password
app.put('/resetpassword/:user_email',async(req,res)=>{
  const {user_email} = req.params
  const {password} = req.body
  // to hash the password
  const salt = bcrypt.genSaltSync(10)
  console.log(salt,password)
  
  const hashedPassword = bcrypt.hashSync(password,salt)

  try {
      const reset = await pool.query('UPDATE users SET hashed_password = $1 WHERE email = $2;',
      [hashedPassword,user_email])

      //password token
      const token = jwt.sign({user_email},'secret',{expiresIn:'1hr'})

      res.json(reset)

  } catch (error) {
      console.error(error)
  }
})

// login
app.post('/loginUser',async(req,res)=>{

  const {user_email,password} = req.body

  try {
      const users = await pool.query('SELECT * FROM users WHERE email = $1',[user_email])
      if(!users.rows.length) return res.json({detail : "User does not exist!" })

      const success = await bcrypt.compare(password,users.rows[0].hashed_password)
      const token = jwt.sign({user_email},'secret',{expiresIn:'1hr'})

      if(success){
          res.json({'user_email' : users.rows[0].email,token})
      }else{
          res.json({detail: "Login failed!"})
      }
  } catch (error) {
      console.error(error)
  }
})
// login
app.post('/login', async (req, res) => {

  const { user_email, password } = req.body
  try {
    const users = await pool.query('SELECT * FROM admin WHERE email = $1', [user_email])
    if (!users.rows.length) return res.json({ detail: "User does not exist!" })
    
    const token = jwt.sign({ user_email }, 'secret', { expiresIn: '1hr' })

    if(password !== users.rows[0].password){
      res.json({ detail: "Password incorrect!" })
    }else{
      res.json({ 'user_email': users.rows[0].email, token })
    }

  } catch (error) {
    console.error(error)
  }
})

// get all users
app.get('/users', async (req, res) => {
  try {
    const users = await pool.query('SELECT * FROM users;')
    res.json(users.rows)

  } catch (error) {
    console.log(error)
  }
})

// get particular user
app.get('/users/:search', async (req, res) => {
  const { search } = req.params
  try {
    const users = await pool.query('SELECT * FROM users WHERE email=$1;', [search])
    res.json(users.rows)

  } catch (error) {
    console.log(error)
  }
})

// Delete a user

app.delete('/delete/:user_email', async (req, res) => {
  const { user_email } = req.params
  try {
    const deleteUser = await pool.query('DELETE FROM users WHERE email = $1;',
      [user_email])
    res.json(deleteUser)
  } catch (error) {
    console.error(error)
  }
})




// get all products
app.get('/products', async (req, res) => {
  try {
    const products = await pool.query('SELECT * FROM products;')
    res.json(products.rows)

  } catch (error) {
    console.log(error)
  }
})
app.delete('/deleteProduct/:id',async(req,res)=>{
  const {id} = req.params
  try {
    const deleteProduct = await pool.query('DELETE FROM products WHERE id=$1;',[id])
    res.json(deleteProduct)
  } catch (error) {
    console.error(error)
  }
})
// get all products
app.get('/products/:id', async (req, res) => {
  const {id} = req.params
  try {
    const products = await pool.query('SELECT * FROM products WHERE id=$1;',[id])
    res.json(products.rows)

  } catch (error) {
    console.log(error)
  }
})



// Handle file upload
app.post('/upload', function (req, res) {
  upload(req, res, async function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(400).json({ message: 'Error uploading file.' });
    }

    // Everything went fine
    console.log(req.file);
    console.log(req.body);
    try {
      const { title, description, price } = req.body;

      const filepaths = req.files['images'].map(file => file.path);


      // insert the post data into the database
      const { rows } = await pool.query(
        'INSERT INTO products (title,description,price,images) VALUES ($1, $2, $3, $4) RETURNING *',
        [title, description, price, filepaths]
      );

      res.status(201).json(rows[0]);
    } catch (err) {
      console.error(err);
      res.status(500).json({ message: 'Server Error' });
    }
  });
});

app.post('/uploadpots', function (req, res) {
  upload(req, res, async function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(400).json({ message: 'Error uploading file.' });
    }

    // Everything went fine
    console.log(req.file);
    console.log(req.body);
    try {
      const { title, description, price } = req.body;

      const filepaths = req.files['images'].map(file => file.path);


      // insert the post data into the database
      const { rows } = await pool.query(
        'INSERT INTO pots (title,description,price,images) VALUES ($1, $2, $3, $4) RETURNING *',
        [title, description, price, filepaths]
      );

      res.status(201).json(rows[0]);
    } catch (err) {
      console.error(err);
      res.status(500).json({ message: 'Server Error' });
    }
  });
});

// Add an item to the cart
app.post('/addtocart/:email', async (req, res) => {
  const {email}= req.params;
  const {title,description,price,images} = req.body;

  try {
    const query = 'INSERT INTO cart_items (title, description,price,images,email) VALUES ($1, $2,$3,$4,$5) RETURNING *';
    const values = [title,description, price,images,email];
    const result = await pool.query(query, values);

    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error('Error adding item to cart:', error);
    res.status(500).json({ error: 'An error occurred while adding the item to the cart.' });
  }
});
// get all cart products of specific users
app.get('/getcartitems/:User_email', async (req, res) => {
  const {User_email} = req.params;
  try {
    const cartItem = await pool.query('SELECT * FROM cart_items WHERE email=$1;',[User_email])
    res.json(cartItem.rows)

  } catch (error) {
    console.log(error)
  }
})

// delete an item from the cart
app.delete('/deleteItem/:id', async (req, res) => {
  const {id} = req.params
  try {
    const deleteItem= await pool.query('DELETE FROM cart_items WHERE id = $1;',
      [id])
    res.json(deleteItem)
  } catch (error) {
    console.error(error)
  }
})

// Add an item to the cart
app.post('/placeOrder/:email', async (req, res) => {
  const {email} = req.params
  const {fullname,contact,location,title,price} = req.body;
  try {
    const query = 'INSERT INTO orders (email,fullname,phone,location,title,price) VALUES ($1,$2,$3,$4,$5,$6) RETURNING *';
    const values = [email,fullname,contact,location,title,price];
    const result = await pool.query(query, values);

    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error('Ordered failed!', error);
    res.status(500).json({ error: 'An error occurred while processing your order.' });
  }
});

app.get('/getOrders',async(req,res)=>{
  try {
    const orders = await pool.query('SELECT * FROM orders;')
    res.json(orders.rows)
  } catch (error) {
    console.error(error)
  }
})
app.delete('/deleteOrder/:id',async(req,res)=>{
  const {id} = req.params
  const deleteOrder = await pool.query('DELETE FROM orders WHERE id=$1;',[id])
  res.json(deleteOrder)
  try {
    
  } catch (error) {
    console.error(error)
  }
})

// post feedbacks
app.post('/sendfeedback',async(req,res)=>{
  const {name,email,feedback}=req.body
  try {
    const sendfeedback = await pool.query('INSERT INTO feedbacks(name,email,feedbacks) VALUES($1,$2,$3);',[name,email,feedback])
    res.json(sendfeedback.rows)
  } catch (error) {
    console.error(error)
  }
})

// get all feedbacks
app.get('/feedbacks', async (req, res) => {
  try {
    const feedbacks = await pool.query('SELECT * FROM feedbacks;')
    res.json(feedbacks.rows)

  } catch (error) {
    console.log(error)
  }
})

// Delete a feedback
app.delete('/deleteFeedback/:user_email', async (req, res) => {
  const {user_email} = req.params
  try {
    const deleteFeedback = await pool.query('DELETE FROM feedbacks WHERE email = $1;',
      [user_email])
    res.json(deleteFeedback)
  } catch (error) {
    console.error(error)
  }
})


app.listen(PORT, () => console.log(`Server running on PORT ${PORT}`))