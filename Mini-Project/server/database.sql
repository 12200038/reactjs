CREATE DATABASE drukflowerplant;

CREATE TABLE users(
    email VARCHAR(255) PRIMARY KEY,
    hashed_password VARCHAR(255)
)
CREATE TABLE admin(
    email VARCHAR(255) PRIMARY KEY,
    password VARCHAR(255)
)

CREATE TABLE feedbacks(
  id SERIAL PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255),
  feedbacks VARCHAR(255)
)

CREATE TABLE products(
  id SERIAL PRIMARY KEY,
  title VARCHAR(150),
  description VARCHAR(1000),
  price VARCHAR(30),
  images text[]
);

CREATE TABLE cart_items(
  id SERIAL PRIMARY KEY,
  title VARCHAR(150),
  description VARCHAR(1000),
  price VARCHAR(30),
  images text[],
  email VARCHAR(200)
);

CREATE TABLE orders(
  id SERIAL PRIMARY KEY,
  email VARCHAR(255),
  fullname VARCHAR(255),
  phone VARCHAR(8),
  location VARCHAR(255),
  title VARCHAR(255),
  price VARCHAR(255)
);