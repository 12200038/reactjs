import logo from './logo.svg';
import './App.css';
import Auth from './components/Auth';
import { useCookies } from 'react-cookie';
import { createContext } from 'react';
import DashBoard from './components/DashBoard';

export const Context = createContext()

function App() {

  const [cookies,setCookie,removeCookie] = useCookies('')
  const authToken = cookies.AuthToken
  const userEmail = cookies.User_email

  return (
    <div className="App">
      <Context.Provider value={userEmail}>
        {!authToken && <Auth/>}

        {authToken && 
          <DashBoard/> 
        }
      </Context.Provider>
     
      
    </div>
  );
}

export default App;
