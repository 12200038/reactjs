import React, { useState } from 'react'
import './modal.css'
import { Box, Container, CssBaseline,Button, TableContainer, TableCell,TableHead,TableRow, Table, TableBody, Paper } from '@mui/material';

function AddUserModal({setShowModal,user}) {
    const [data,setData] = useState({
        user_email:user.email,
        password:user.password
    }) 


    // ***Posting data into the database***   VERY IMPORTANT

    const postData = async(e)=>{
        e.preventDefault()
        try {
            const response = await fetch('http://localhost:8000/signup',{
                method:'POST',
                headers:{'Content-Type':'application/json'},
                body:JSON.stringify(data)
            })
   
            if(response.status === 200){
                setShowModal(false)
            }
        } catch (error) {
            console.error(error)
        }
    }
    
    const handleChange=(e)=>{
        const {name,value} = e.target
        setData(data=>({
            ...data,
            [name]:value
        }))
        console.log(data)
    }
    
  return (
    <div className='overlay'>
        <div className='add-user-container'>
            <div className='header'>
                <span>Add user</span>
                <span style={{cursor:'pointer'}} onClick={()=>setShowModal(false)}>X</span>
            </div>
            <div className='form-container'>
                <form>
                    <input type='text' placeholder='Email' name='user_email' value={data.user_email} required onChange={handleChange}/>
                    <input type="password" placeholder="Password" name='password' value={data.password} onChange={handleChange}/>
                    <button onClick={postData} type='submit'>Submit</button>
                </form>
            </div>
        </div>
    </div>
  )
}

export default AddUserModal