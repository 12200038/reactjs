import React from 'react'
import './alert.css'
function ProductAlert({setShowAlert,products}) {

    const deleteProduct = async(e)=>{
        e.preventDefault()
      try {
        const response = await fetch(`http://localhost:8000/deleteProduct/${products.id}`,{
          method:'DELETE'
        })
      } catch (error) {
        console.error(error)
      }
      setShowAlert(false)
    }
    // const handleDelete=()=>{
    //     deleteProduct()
    //     setShowAlert(false)
    // }
  return (
    <div className='overlays'>
         <div className='alert'>
            <h2 style={{color:'red'}}>Alert!</h2>
            <p>Are you sure you want to delete this product?</p>
            <div className='btns'>
                <span onClick={deleteProduct}>Yes</span>
                <span onClick={()=>setShowAlert(false)}>No</span>
            </div>
        </div>

    </div>
  )
}

export default ProductAlert