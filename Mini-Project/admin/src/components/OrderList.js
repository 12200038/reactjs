import React, { useState,useContext } from 'react'
// import './OrderList.css'
import {TableCell,TableRow } from '@mui/material'
import OrderAlert from './OrderAlert'

function OrderList({order}) {
  const [showAlert,setShowAlert ]= useState(false)
    return (
        <TableRow sx={{paddingLeft:'20px'}}>
        <TableCell style={{color:'#6c757d'}}>{order.email}</TableCell>
        <TableCell style={{color:'#6c757d'}}>{order.fullname}</TableCell>
        <TableCell style={{color:'#6c757d'}}>{order.phone}</TableCell>
        <TableCell style={{color:'#6c757d'}}>{order.location}</TableCell>
        <TableCell style={{color:'#6c757d'}}>{order.title}</TableCell>
        <TableCell style={{color:'#6c757d'}}>{order.price}/-</TableCell>
        <TableCell style={{color:'#6c757d'}}>
        <button onClick={()=>setShowAlert(true)} className='del-user'>
          <span className='btn-text-one'>Delete</span>
         <span className='btn-text-two'>Confirm!</span>
     </button>
        </TableCell>
        {showAlert && <OrderAlert setShowAlert={setShowAlert} order={order}/>}
      </TableRow>
  )
}

export default OrderList