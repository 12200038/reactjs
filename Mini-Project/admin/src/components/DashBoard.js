import React, { useState, useEffect, createContext, useContext } from 'react';
import './dashboard.css'
import Navbar from './Navbar';
import UserList from './UserList';
import AddUserModal from './AddUserModal';
import FeedbackList from './FeedbackList';
import AddItemModal from './AddItemModal';
import ProductList from './ProductList';
import { Box, Container, CssBaseline, Button, TableContainer, TableCell, TableHead, TableRow, Table, TableBody, Paper, Hidden } from '@mui/material';
import { Context } from '../App';
import { useCookies } from 'react-cookie';
import OrderList from './OrderList';

export const HeaderContext = createContext()

const DashBoard = () => {
  const currentUser = useContext(Context)

  const [activeTab, setActiveTab] = useState('orders');
  const [showModal, setShowModal] = useState(false)
  const [users, setUsers] = useState([])
  const [feedbacks, setFeedbacks] = useState([])
  const [products, setProducts] = useState([])
  const [orders, setOrders] = useState([])
   const [searchItem, setSearchItem] = useState('')
  const [cookies, setCookie, removeCookie] = useCookies('')

  const [searchQuery, setSearchQuery] = useState('');
  const [filteredUsers, setFilteredUsers] = useState([]);
  const [filteredProduct, setFilteredProduct] = useState([]);

  // search user
  const handleSearch = (event) => {
    setSearchQuery(event.target.value);
  };

  useEffect(() => {
    if (users) {
      const filteredItems = users.filter((item) =>
        item.email.toLowerCase().includes(searchQuery.toLowerCase())
      );
      setFilteredUsers(filteredItems);
    }
  }, [users, searchQuery]);


  // search product
  const handleSearchProduct = (event) => {
    setSearchQuery(event.target.value);
  };

  useEffect(() => {
    if (products) {
      const filteredItems = products.filter((item) =>
        item.title.toLowerCase().includes(searchQuery.toLowerCase())
      );
      setFilteredProduct(filteredItems);
    }
  }, [products, searchQuery]);




  const handleTabClick = (tab) => {
    setActiveTab(tab);
  };

  //get the list of users who have registered in our app
  const getUsers = async () => {
    try {
      const response = await fetch('http://localhost:8000/users')
      const json = await response.json()
      setUsers(json)

    } catch (error) {
      console.error(error)
    }
  }


  //get the list of feedbacks given by the users
  const getFeedbacks = async () => {
    try {
      const response = await fetch('http://localhost:8000/feedbacks')
      const json = await response.json()
      setFeedbacks(json)

    } catch (error) {
      console.error(error)
    }
  }

  //get the list of products
  const getProducts = async () => {
    try {
      const response = await fetch('http://localhost:8000/products')
      const json = await response.json()
      setProducts(json)

    } catch (error) {
      console.error(error)
    }
  }

  const getOrders = async () => {
    try {
      const response = await fetch('http://localhost:8000/getOrders')
      const json = await response.json()
      setOrders(json)
    } catch (error) {
      console.error(error)
    }
  }
  // logout
  const logout = () => {
    removeCookie('User_email')
    removeCookie('AuthToken')

    window.location.reload()
  }


  useEffect(() => {
    getUsers()
  }, [])

  useEffect(() => {
    getFeedbacks()
  }, [])

  useEffect(() => {
    getProducts()
  }, [])

  useEffect(() => {
    getOrders()
  }, [])

  return (
    <Container sx={{ display: 'flex', justifyContent: 'space-between' }} maxWidth={false} style={{ height: '100%' }}>
      <CssBaseline />
      <Box sx={{ paddingBottom: '30px', width: '300px', height: '800px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
        <div style={{ marginTop: '20px', display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
          <Box sx={{ marginBlockEnd: '20px', }}>
            <img src='logo.png' height='93' width='100' />
            <h4>Admin Panel</h4>
          </Box>

          <Box>
            <button
              className={`tab ${activeTab === 'orders' ? 'active' : ''}`}
              onClick={() => handleTabClick('orders')}
            >
              <img src='order.png' width='20' height='20' />
              Orders
            </button>
            <button
              className={`tab ${activeTab === 'users' ? 'active' : ''}`}
              onClick={() => handleTabClick('users')}
            >
              <img src='users.png' width='20' height='20' />
              Users
            </button>
            <button
              className={`tab ${activeTab === 'products' ? 'active' : ''}`}
              onClick={() => handleTabClick('products')}
            >
              <img src='product.png' width='20' height='20' />
              Products
            </button>

            <button
              className={`tab ${activeTab === 'feedback' ? 'active' : ''}`}
              onClick={() => handleTabClick('feedback')}
            >
              <img src='feedback.png' width='20' height='20' />
              Feedback
            </button>
          </Box>
        </div>

        <Container style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
          <Box className='useracc'>
            <span style={{ fontSize: '12px' }}><strong>{currentUser}</strong></span>

          </Box>
          <button className='logout' onClick={logout}>Logout</button>
        </Container>

      </Box>


      <Box sx={{ width: '100%', margin: '20px' }}>
        <HeaderContext.Provider value={activeTab}>
          <Navbar activeTab={activeTab} />
        </HeaderContext.Provider>

        {/* orders */}
        {activeTab === 'orders' && (
          <Paper sx={{ maxWidth: '100%', marginBlock: '10px' }}>
            <TableContainer component={Paper} sx={{ maxHeight: '650px' }}>
              <Table>

                <TableHead >
                  <TableRow style={{ position: 'sticky', top: '0', backgroundColor: 'grey', zIndex: '1' }}>
                    <TableCell sx={{ textAlign: 'center', color: 'white' }}>Email</TableCell>
                    <TableCell sx={{ borderWidth: '0px', color: 'white' }}>Cust. Name</TableCell>
                    <TableCell sx={{ textAlign: 'center', color: 'white' }}>Contact</TableCell>
                    <TableCell sx={{ borderWidth: '0px', color: 'white' }}>Location</TableCell>
                    <TableCell sx={{ textAlign: 'center', color: 'white' }}>Item</TableCell>
                    <TableCell sx={{ borderWidth: '0px', color: 'white' }}>Amount (Nu)</TableCell>
                    <TableCell sx={{ borderWidth: '0px', color: 'white' }}>Action</TableCell>

                  </TableRow>
                </TableHead>
                {/* orderlist is here */}
                <TableBody style={{ overflow: 'scroll' }}>
                  {orders.map((order) => (
                    <OrderList order={order} key={order.id} />
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            {showModal && <AddUserModal setShowModal={setShowModal} user={users} />}
          </Paper>
        )}


        {/* users */}
        {activeTab === 'users' && (

          <Paper sx={{ maxWidth: '100%', marginBlock: '10px' }}>
            <TableContainer component={Paper} sx={{ maxHeight: '650px' }}>
              <Table>

                <TableHead >
                  <TableRow style={{ position: 'sticky', top: '0', backgroundColor: 'grey', zIndex: '1' }}>
                    <TableCell sx={{ textAlign: 'center', color: 'white' }}>Email</TableCell>

                    <TableCell sx={{ borderWidth: '0px', color: 'white' }}>ACTION</TableCell>
                    <TableCell sx={{ borderWidth: '0px', display: 'flex', alignItems: 'center', justifyContent: 'space-evenly' }}>
                      <button className='add-user-btn' onClick={setShowModal}>Add</button>
                      <input className="form-control mr-sm-2" style={{ width: 300 }} type="search" placeholder="Search" aria-label="Search" value={searchQuery} onChange={handleSearch} />
                    </TableCell>
                  </TableRow>
                </TableHead>
                {/* userlist is here */}
                <TableBody style={{ overflow: 'scroll' }}>
                  {filteredUsers.map((user) => (
                    <UserList user={user} key={user.id} />
                  ))}

                </TableBody>
              </Table>
            </TableContainer>
            {showModal && <AddUserModal setShowModal={setShowModal} user={users} />}
          </Paper>

        )}

        {/* products */}
        {activeTab === 'products' && (
          <Paper sx={{ maxWidth: '100%', marginBlock: '10px' }}>
            <TableContainer component={Paper} sx={{ maxHeight: '650px' }}>
              <Table>

                <TableHead>
                  <TableRow style={{ position: 'sticky', top: '0', backgroundColor: 'grey' }}>
                    <TableCell sx={{ textAlign: 'center', color: 'white' }}>NAME</TableCell>
                    <TableCell sx={{ borderWidth: '0px', color: 'white' }}>ACTION</TableCell>
                    <TableCell sx={{ borderWidth: '0px', display: 'flex', alignItems: 'center', justifyContent: 'space-evenly' }}>
                      <button className='add-user-btn' onClick={setShowModal}>Add</button>
                      <input className="form-control mr-sm-2" style={{ width: 300 }} type="search" placeholder="Search" aria-label="Search" value={searchQuery} onChange={handleSearchProduct} />
                    </TableCell>
                  </TableRow>
                </TableHead>

                {/* productlist is here */}
                <TableBody style={{ overflow: 'scroll' }}>
                  {filteredProduct.map((products) => (
                    <ProductList key={products.id} products={products} setShowModal={setShowModal} />
                  ))}
                </TableBody>

              </Table>
            </TableContainer>

            {showModal && <AddItemModal setShowModal={setShowModal} />}

          </Paper>
        )}

        {/* feedbacks */}
        {activeTab === 'feedback' && (
          <Paper sx={{ maxWidth: '100%', marginBlock: '10px' }}>
            <TableContainer component={Paper} sx={{ maxHeight: '650px' }}>
              <Table>

                <TableHead style={{ backgroundColor: 'grey' }}>
                  <TableRow>
                    <TableCell sx={{ textAlign: 'center', color: '#fff' }}>EMAIL</TableCell>
                    <TableCell sx={{ borderWidth: '0px', color: '#fff' }}>ACTION</TableCell>

                  </TableRow>
                </TableHead>
                {/* feedlist is here */}
                <TableBody style={{ overflow: 'scroll' }}>
                  {feedbacks.map((feedback) => (
                    <FeedbackList key={feedback.email} feedback={feedback} setShowModal={setShowModal} />
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

          </Paper>
        )}
      </Box>
    </Container>
  );
};

export default DashBoard;
