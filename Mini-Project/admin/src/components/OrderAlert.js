import React from 'react'

function OrderAlert({order,setShowAlert}) {
    const deleteOrder = async(e)=>{
        e.preventDefault()
      try {
        const response = await fetch(`http://localhost:8000/deleteOrder/${order.id}`,{
          method:'DELETE'
        })
      } catch (error) {
        console.error(error)
      }
      setShowAlert(false)
      window.location.reload()
    }

  return (
    <div className='overlays'>
         <div className='alert'>
            <h2 style={{color:'red'}}>Alert!</h2>
            <p>Are you sure you want to delete this order?</p>
            <div className='btns'>
                <span onClick={deleteOrder}>Yes</span>
                <span onClick={()=>setShowAlert(false)}>No</span>
            </div>
        </div>

    </div>
  )
}

export default OrderAlert