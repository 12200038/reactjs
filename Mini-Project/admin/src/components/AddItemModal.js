import React, { useState, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import ModalImage from 'react-modal-image';
import axios from 'axios';
import './modal.css'

export default function AddItemModal({setShowModal,products}) {
    
    const [uploadStatus, setUploadStatus] = useState('');
    // const descriptionRef = useRef(null); // add a ref to the textarea
    const [canUpload, setCanUpload] = useState(false);
    
    const [title,setTitle] =useState('');
    const [description,setDescription] =useState('');
    const [price,setPrice] =useState('');
    const [images, setImages] = useState([]);

    const handleUpload = async () =>{
        try {
          setUploadStatus('Uploading...'); // set upload status to uploading
    
          const formData = new FormData(); // create a new FormData object
          formData.append('title', title); 
          formData.append('description', description);
          formData.append('price', price);
          images.forEach((image) => {
            formData.append('images', image); // add each image to the form data
          });
    
          // make the API call using axios
          const response = await axios.post('http://localhost:8000/upload', formData, {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          });
    
          setUploadStatus('Upload successful!'); // set upload status to successful
          setShowModal(false)
          window.location.reload();

        } catch (error) {
          console.error(error);
          setUploadStatus('Upload failed. Please try again.');
        }
      };

    const onDrop = acceptedFiles => {

      const newImages = [...images, ...acceptedFiles];
      // Limit the number of images to 20
      if (acceptedFiles.length + images.length > 20) {
        alert('You can only upload a maximum of 20 images');
        return;
      }
      setImages(newImages.slice(0, 20));
    };

    const { getRootProps, getInputProps } = useDropzone({ onDrop });
    useEffect(() => {
        if (images.length > 0) {
          setCanUpload(true);
        } else {
          setCanUpload(false);
        }
      }, [title,description,price,images]);

  return (
    <div className="overlay">
      <div className='main-container'>
        <div className='additem-header'>
          <span>Add Item</span>
          <button onClick={()=>setShowModal(false)} style={{cursor:'pointer'}} ><img src='./close.png' width='15' height='15' /></button>
        </div>

        <div className='add-detail-container'>
          {/* image */}
          <div className='image-container'>
            <div className='img-preview' style={{height:450}}>
              {images.map((image, index) => (
                
                  <img key={index} src={URL.createObjectURL(image)} style={{width:'100%',height:450}}/>
                  

              ))}
            </div>
            <div className='choose-btn'>
              <div {...getRootProps()} 
              className='choosefile'
                >
                <input {...getInputProps()}/>
                Choose file
              </div>
            </div>
            
          </div>

          {/* inputs */}
          <div className='input-container'>
            <h6 style={{color:'#333',marginBottom:'20px', fontWeight:'bold',fontSize:'18px'}}>Add Details</h6>
            <input required placeholder='Title'  type='text' value={title} onChange={(e)=>{setTitle(e.target.value)}} />
            <textarea required placeholder='Description'  type='text'  value={description} onChange={(e)=>{setDescription(e.target.value)}} />
            <input required placeholder='Price'  type='text' size="sm" value={price} onChange={(e)=>{setPrice(e.target.value)}} />
            {canUpload && title!=="" && description!=="" && price!=="" ? 
              <button onClick={handleUpload}>Upload</button>
                :
              <button onClick={handleUpload} disabled>Upload</button>   
            }
          </div>
          

        </div>
      </div>
  
    
    </div>
  )
}
