import React from 'react'
import './alert.css'

function UserAlert({setShowAlert,user}) {

    const deleteUser = async(e)=>{
          e.preventDefault()
        try {
          const response = await fetch(`http://localhost:8000/delete/${user.email}`,{
            method:'DELETE'
          })
        } catch (error) {
          console.error(error)
        }
        setShowAlert(false)
        window.location.reload()
      }
  return (
    <div className='overlays'>
         <div className='alert'>
            <h2 style={{color:'red'}}>Alert!</h2>
            <p>Are you sure you want to delete this user?</p>
            <div className='btns'>
                <span onClick={deleteUser}>Yes</span>
                <span onClick={()=>setShowAlert(false)}>No</span>
            </div>
        </div>
    </div>
  )
}

export default UserAlert