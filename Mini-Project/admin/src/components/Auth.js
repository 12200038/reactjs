import React, { useState } from 'react'
import {useCookies} from 'react-cookie'
// import './auth.css'
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

function Copyright(props) {
    return (
      <Typography variant="body2" color="text.secondary" align="center" {...props}>
        {'Copyright © '}
        <Link color="inherit" href="https://mui.com/">
          Your Website
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
}

const theme = createTheme();

function Auth() {
    const [cookies,setCookie,removeCookie]=useCookies('')
   
    const [error,setError]=useState('')
   
    const [user_email,setUser_email]=useState('')
    const [password,setPassword]=useState('')
   



    const handleSubmit = async(e)=>{

        e.preventDefault()


        const response = await fetch('http://localhost:8000/login',{
            method:'POST',
            headers:{'Content-Type':'application/json'},
            body:JSON.stringify({user_email,password})

        })
        const data = await response.json()

        if(data.detail){
            setError(data.detail)
        }else{
            setCookie('User_email',data.user_email)
            setCookie('AuthToken',data.token) 

            window.location.reload()
        }
    }

  return (
    <div style={{paddingBlock:'35px'}}>
        <ThemeProvider theme={theme}>
        <Container component='main' maxWidth='xs'>
            <CssBaseline/>
            <Box>
                <img src='logo.png' height='93' width='100'/>
                <h4 style={{color:'#333'}}>Admin Panel</h4>
            </Box>

            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              style={{width:'350px'}}
              id="email"
              label="Email"
              name="email"
              autoComplete="email"
              autoFocus
              size='small'
              onChange={(e)=>setUser_email(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              style={{width:'350px'}}
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              size='small'
              autoComplete="current-password"
              onChange={(e)=>setPassword(e.target.value)}
            />

            {error && <p style={{color:'#d61f2c'}}>{error}</p>}
            <Button
              type="submit"
              style={{width:'350px',backgroundColor:'#3a564c',color:'white'}}
              sx={{ mt: 3, mb: 2 }}
              onClick={handleSubmit}
            >
              Login
            </Button>
            
          </Box> 
          <Copyright sx={{ mt: 8, mb: 4 }} />
        </Container>
    </ThemeProvider>
    
    </div>
    

            
           
  )
}

export default Auth