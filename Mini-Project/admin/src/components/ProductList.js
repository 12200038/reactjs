import React, { useState } from 'react'
import ItemDetails from './ItemDetails'
import { Box,Button, Container, TableBody, TableCell, TableContainer, TableRow } from '@mui/material'
import ProductAlert from './ProductAlert'

function ProductList({products}) {
    const [showProductDetail,setShowProductDetail] = useState(false)
    const [showAlert,setShowAlert]=useState(false)

    

  return (

     <TableRow sx={{paddingLeft:'20px'}}>
          <TableCell style={{color:'#6c757d'}}>
            {products.title}
        </TableCell>
        {/* <TableCell>
          <Button style={{borderRadius:'15px',textTransform:'capitalize',width:'70px',height:'30px'}} size='small' variant='outlined' onClick={()=>setShowProductDetail(true)}>View</Button>
       

        </TableCell> */}
        <TableCell style={{display:'flex',justifyContent:'flex-start',alignItems:'center'}}>
            <Button style={{borderRadius:'15px',textTransform:'capitalize',width:'70px',height:'30px',marginRight:'10px'}} size='small' variant='outlined' onClick={()=>setShowProductDetail(true)}>View</Button>
            <button onClick={()=>setShowAlert(true)} className='del-user'>
                <span className='btn-text-one'>Delete</span>
                <span className='btn-text-two'>Confirm!</span>
            </button>
        </TableCell>
        {showAlert && <ProductAlert setShowAlert={setShowAlert} products={products}/>}
        <TableCell></TableCell>
        
        {showProductDetail && <ItemDetails image={products.images} title={products.title} description={products.description} id={products.id} price={products.price} setShowProductDetail={setShowProductDetail} />}
    </TableRow>
    

     
      
   
 
    
  )
}

export default ProductList