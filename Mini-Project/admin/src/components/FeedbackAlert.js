import React from 'react'
import './alert.css'
function FeedbackAlert({setShowAlert,feedback}) {

    
  const deleteFeedback = async(e)=>{
    e.preventDefault()
    try {
        const response = await fetch(`http://localhost:8000/deleteFeedback/${feedback.email}`,{
            method:"DELETE",
            headers:{'Content-Type':'application/json'},
            body:JSON.stringify(feedback)
        })
    } catch (error) {
        console.error(error)
    }
    setShowAlert(false)
    window.location.reload()
}

  return (
    <div className='overlays'>
         <div className='alert'>
            <h2 style={{color:'red'}}>Alert!</h2>
            <p>Are you sure you want to delete this feedback!</p>
            <div className='btns'>
                <span onClick={deleteFeedback}>Yes</span>
                <span onClick={()=>setShowAlert(false)}>No</span>
            </div>
        </div>

    </div>
  )
}

export default FeedbackAlert