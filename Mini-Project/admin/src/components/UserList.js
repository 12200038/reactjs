import React, {useState, useContext } from 'react'
import './userlist.css'
import { Button, TableCell, TableBody,TableRow } from '@mui/material'
import { UserContext } from './DashBoard'
import UserAlert from './UserAlert'

function UserList({user}) {

  const [showAlert,setShowAlert ]= useState(false)

  return (
     <TableRow sx={{paddingLeft:'20px'}}>
     <TableCell style={{color:'#6c757d'}}>
       {user.email}
   </TableCell>
   <TableCell>
   <button onClick={()=>setShowAlert(true)} className='del-user'>
          <span className='btn-text-one'>Delete</span>
         <span className='btn-text-two'>Confirm!</span>
     </button>
   </TableCell>
   {showAlert && <UserAlert setShowAlert={setShowAlert} user={user}/>}
   <TableCell></TableCell>
   </TableRow>
  )
}

export default UserList