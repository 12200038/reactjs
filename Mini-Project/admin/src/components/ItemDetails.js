import React from 'react'
import './ItemDetail.css'
function ItemDetails({title,description,id,setShowProductDetail,price,image}) {
  return (
    <div className='overlay'>
        <div className='itemcontainer'>
            <div className='closedetail'>
                <span>Item Detail</span>
                <button onClick={()=>setShowProductDetail(false)}><img src='./close.png' width='15' height='15' style={{color:'#333'}}/></button>
            </div>
            <div className='detail-container'>
                <div className='itemId'>
                    <img src={`http://localhost:8000/${image[0]}`} style={{height:350}}/>
                    <span>Product ID: {id}</span>
                </div>
                
                <div className='detail'>
                    <div className='item-header'>
                        <span><strong>{title}</strong></span>
                        <span>NU. {price} /-</span>
                    </div>
                    
                    <div className='desc'>
                        <p>{description}</p>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
  )
}

export default ItemDetails