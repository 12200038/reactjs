import React, { useContext, useState } from 'react';
import './navbar.css'
import { useCookies } from 'react-cookie';
import { AppBar, Box, Button, Container, TextField} from '@mui/material';
import { Context } from '../App';
import { HeaderContext } from './DashBoard';

const Navbar = () => {
  const activeTab = useContext(HeaderContext)
  return (
    <Container sx={{paddingBlock:'10px',display:'flex',alignItems:'center',justifyContent:'space-between'}}>
      <div className='header-container'>
        <span>{activeTab}</span>
      </div>
    </Container>
  );
};

export default Navbar;
