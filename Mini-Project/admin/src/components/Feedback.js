import React from 'react'
import './ItemDetail.css'
import './feedback.css'
function Feedback({email,feedback,setShowFeedback}) {
  return (
    <div className='overlay'>
        <div className='feedback-container'>
            <div className='closebtn'>
                <span><strong>{email}</strong></span>
                <button onClick={()=>setShowFeedback(false)}><img src='./close.png' width='15' height='15' style={{color:'#333'}}/></button>
            </div>
            <div className='feedback-detail'>
                    <p>
                        {feedback}
                    </p>
                
            </div>
        </div>
        
    </div>
  )
}

export default Feedback