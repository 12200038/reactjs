import React, { useContext } from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TableVirtuoso } from 'react-virtuoso';
import { UserContext } from './';
import { Button } from '@mui/material';

function UserTable() {

  const users = useContext(UserContext)
  return (

          <TableBody>
            {users.map((user)=>(
              <TableRow key={user.id}>
                  <TableCell>{user.email}</TableCell>
                  <TableCell>Action</TableCell>
              </TableRow>
            ))}
          </TableBody>
  )
}

export default UserTable