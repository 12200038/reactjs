import React, { useState } from 'react'
import Feedback from './Feedback'
import { Box,Button, Container, TableBody, TableCell, TableContainer, TableRow } from '@mui/material'
import './userlist.css'
import FeedbackAlert from './FeedbackAlert'

function FeedbackList({feedback}) {
  const [showFeedback,setShowFeedback] = useState(false)
  const [showAlert,setShowAlert ]= useState(false)

  return (

     <TableRow sx={{paddingLeft:'20px'}}>
          <TableCell style={{color:'#6c757d'}}>
            {feedback.email}
        </TableCell>
        <TableCell style={{display:'flex',justifyContent:'flex-start',alignItems:'center'}}>
            <Button style={{borderRadius:'15px',textTransform:'capitalize',width:'70px',height:'30px',marginRight:'10px'}} size='small' variant='outlined' onClick={()=>setShowFeedback(true)}>View</Button>
            <button onClick={()=>setShowAlert(true)} className='del-user'>
                <span className='btn-text-one'>Delete</span>
                <span className='btn-text-two'>Confirm!</span>
            </button>
        </TableCell>
        {showAlert && <FeedbackAlert setShowAlert={setShowAlert} feedback={feedback}/>}
        {showFeedback && <Feedback email={feedback.email} feedback={feedback.feedbacks} setShowFeedback={setShowFeedback} />}
    </TableRow>
    
  )
}

export default FeedbackList